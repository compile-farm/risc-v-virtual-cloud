# RISC-V virtual cloud

The goal of this project is to provide access to RISC-V virtual machines
to all users of the [GCC compile farm](https://cfarm.tetaneutral.net).

This is made possible by:

- high-end physical hardware provided to the compile farm by [OSUOSL](http://osuosl.org/)
- support for [RISC-V emulation in QEMU](https://wiki.qemu.org/Documentation/Platforms/RISCV)

For now, we target Fedora and Debian, but we might add other systems
such as FreeBSD in the future.


# Using the RISC-V virtual cloud

If you just want to use the RISC-V virtual machines, just connect to them over SSH
like regular machines, see <https://cfarm.tetaneutral.net/machines/list/>.

The machines are reachable over custom SSH ports, there is a button "Show SSH config" in the page
above to simplify your local SSH setup.

**The RISC-V emulated machines are still experimental and not listed on the main compile farm website**.
If you want to connect anyway, without any warranty, use:

- gcc401.fsffrance.org port 45061 : Debian
- gcc402.fsffrance.org port 45062 : Debian
- gcc403.fsffrance.org port 45063 : Fedora


# How it works: basic principle

We create a base RISC-V system, and then convert it to a Docker image.  This RISC-V Docker image
can be run out-of-the-box on a x86_64 system, provided `qemu-user` is installed with the appropriate
`binfmt` configuration.

We futher customize these Docker images for gccfarm usage:

- mount `/home` from the host, so that all user files are accessible
- create all farm user accounts (`/etc/passwd` etc)
- add known SSH host keys, so that we can rebuild an image without creating SSH warnings on clients
- install development packages: compilers, text editors, libraries...

The rest of this README describes in more details how we create the base image and how we customize it.


# First step: RISC-V containers

Thanks to `qemu-user-static`, it's very easy to build and run containers with a different architecture.
It's more limited than a full virtual machine, but for many usages this may be sufficient.

## Building the containers

- we create a riscv64 Debian chroot using <https://wiki.debian.org/RISC-V#Creating_a_riscv64_chroot>
- we import the chroot in a docker image
- we customize the docker image with a Dockerfile

This is done in `docker/debian-riscv64/`.

If you want to build the containers yourself, you need a recent Debian (tested on bullseye, buster might work).

Install the required packages to create the riscv64 chroot:

    apt install mmdebstrap qemu-user-static binfmt-support debian-ports-archive-keyring

And the docker system:

    apt install docker.io docker-compose

Then run each build step:

    # docker/debian-riscv64/1-debian-riscv64-base.sh
    # docker/debian-riscv64/2-debian-riscv64-gccfarm-min.sh
    # docker/debian-riscv64/3-debian-riscv64-gccfarm-full.sh

Each step uses the docker image created by the previous step, and generates a new docker image.
It means that you can customize a step and re-run only this step to save build time.

The docker images created by each step are:

1) `debian-riscv64-base`: just the chroot, nothing else. A really basic system.
2) `debian-riscv64-gccfarm-min`: minimum system to be useful in the gcc compile farm, with a SSH server.
3) `debian-riscv64-gccfarm-full`: additionally contains many useful Debian packages for developement. It's much bigger and takes a long time to build.

There is also a CI pipeline (see `.gitlab-ci.yaml` in this repo) that implements the same idea.

## Running the containers locally

To run one of the images locally, make sure you have qemu and binfmt:

    apt install qemu-user-static binfmt-support

And then you can simply use the classical docker command:

    docker run -it debian-riscv64-gccfarm-min /bin/bash

You have a root shell in the container.  All binaries are run automatically
with `qemu-riscv64-static`.

## Running the containers for the farm

Containers are stateless, and we plan to update them regularly. As such, any persistent data
needs to be stored outside of the container.  Fortunately, Docker makes it easy to share files
between host and containers.

We share these paths with the host:

- `/home`
- `/etc/passwd`, `/etc/group`, `/etc/shadow`

And we provide additional persistent directories (not the same as the host):

- `/var/spool/cron`

We also need to configure SSH access.  Docker typically isolates the network of containers
and expose specific ports from the host, but in the farm we need to bridge the containers to the
physical network.  This is done with the `macvlan` driver.

To do all that, we use docker-compose, see `docker/docker-compose-common.yml`.
The actual container definitions are in `docker/osuosl-riscv-gcc140/docker-compose.yml`.

To start the containers:

    # cd docker/osuosl-riscv-gcc140
    # docker-compose up -d

When you rebuild the images, simply run `docker-compose up -d` again (from the right directory!)
to restart the containers.

## Remaining work

Crontabs are stored persistently, but currently no cron daemon is running in the containers,
so the crontabs don't have any effect.  It could be solved by running systemd as the main
process of the container (instead of sshd currently), but this approach has not been
successful so far.

Builds could be sped up by using a HTTP cache for downloaded packages (see man docker-build).
Building the docker images in a ramfs should also greatly speed up package installation.

## Limitations

User-mode emulation is faster and more stable than system-mode emulation, but it has some limitations:

- `strace` and `gdb` don't work inside the container. It might still possible to debug a program
  by running qemu directly (it has a `-strace` option and a built-in gdbserver, thanks to
  Andreas Schwab for the tip)

Additionally, the gccfarm setup has the following limitations:

- no services are running in the container (as mentioned above, the most-needed service would be crond)
- in some cases, upgrading packages on the host will "unbind" the bind mounts of the container (`/etc/passwd`, etc).
  When this happens, the container will be stuck with an "frozen" copy of `/etc/passwd` and will no longer see
  updates to this file done by the host.  Basically, the host switched to a new inode while the container is stuck
  with the old inode.  Restarting the container is enough to pick up the new inode.

# Second step: full RISC-V virtual machines

The idea of this second step is to run qemu in system mode and boot a complete RISC-V system.

This is a work-in-progress, we need appropriate tools to create and customize a virtual machine:

- Debian doc to setup a riscv64 VM: <https://wiki.debian.org/RISC-V#Setting_up_a_riscv64_virtual_machine>
- `virt-customize` / `libguestfs-tools` to manipulate / customize the VM image
- kameleon <http://kameleon.imag.fr> to write reproducible recipes

Sharing directories is more difficult than with Docker, we will likely need NFS.

