#!/bin/sh

set -e

SUITE=sid
MIRROR1="deb http://deb.debian.org/debian-ports/ sid main"
MIRROR2="deb http://deb.debian.org/debian-ports/ unreleased main"

echo "* Creating chroot tarball and importing it in Docker..."

mmdebstrap \
  --architectures=riscv64 \
  --include="debian-ports-archive-keyring" \
  $SUITE \
  "-" \
  "$MIRROR1" \
  "$MIRROR2" \
  | docker image import -c "CMD /bin/bash" - debian-riscv64-base:latest
