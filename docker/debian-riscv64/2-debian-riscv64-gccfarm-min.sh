#!/bin/sh

set -e

SCRIPTDIR=$(dirname "$0")

BASE_IMAGE=${1:-debian-riscv64-base:latest}
TARGET_IMAGE=${2:-debian-riscv64-gccfarm-min:latest}

# This is here for documentation purpose: it can be overriden with an env var.
ROOT_SSH_KEY="${ROOT_SSH_KEY:-}"

echo "* Customizing docker image $BASE_IMAGE..."

CONTEXT="$(mktemp -d)"
[ -z "$CONTEXT" ] && exit 1

# Build motd from template
sed -e "s#%IMAGE%#${TARGET_IMAGE}#g" \
    -e "s#%BUILD_DATE%#$(date +%Y-%m-%d)#g" \
    -e "s#%SOURCE%#https://framagit.org/compile-farm/risc-v-virtual-cloud#g" \
    -e "s#%HOST%#gcc140#g" \
    "$SCRIPTDIR"/../motd > "$CONTEXT"/motd

# Add SSH host keys
cp -p "$SCRIPTDIR"/../ssh-host-keys/ssh* "$CONTEXT"/

# Add root authorized_keys
echo "$ROOT_SSH_KEY" > "$CONTEXT"/authorized_keys

cat > "$CONTEXT"/Dockerfile <<EOF
FROM $BASE_IMAGE

# Copy SSH host keys. Needs to be done before installing openssh-server,
# otherwise we may partially overwrite the keys that are automatically generated.
ADD ssh* /etc/ssh/

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y \
  openssh-server \
  ncurses-term \
  locales \
  && rm -rf /var/lib/apt/lists/* /var/cache/apt/archives/*.deb

RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && locale-gen

# Necessary to run sshd directly
RUN mkdir -p /run/sshd

# Copy authorized_keys for root
RUN mkdir -p /root/.ssh
ADD authorized_keys /root/.ssh/authorized_keys

# motd prompt when connecting over SSH
ADD motd /etc/motd
EOF

docker build -t "$TARGET_IMAGE" "$CONTEXT"
rm -r "$CONTEXT"

echo "* Generated docker image $TARGET_IMAGE"
