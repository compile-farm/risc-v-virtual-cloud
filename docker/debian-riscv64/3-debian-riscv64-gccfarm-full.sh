#!/bin/sh

set -e

SCRIPTDIR=$(dirname "$0")

BASE_IMAGE=${1:-debian-riscv64-gccfarm-min:latest}
TARGET_IMAGE=${2:-debian-riscv64-gccfarm-full:latest}

echo "* Customizing docker image $BASE_IMAGE..."

CONTEXT="$(mktemp -d)"
[ -z "$CONTEXT" ] && exit 1

# Build motd from template
sed -e "s#%IMAGE%#${TARGET_IMAGE}#g" \
    -e "s#%BUILD_DATE%#$(date +%Y-%m-%d)#g" \
    -e "s#%SOURCE%#https://framagit.org/compile-farm/risc-v-virtual-cloud#g" \
    -e "s#%HOST%#gcc140#g" \
    "$SCRIPTDIR"/../motd > "$CONTEXT"/motd

cat > "$CONTEXT"/Dockerfile <<EOF
FROM $BASE_IMAGE
RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y \
  bc \
  bash-completion \
  csh \
  tcsh \
  zsh \
  htop \
  iputils-ping \
  net-tools \
  mtr-tiny \
  traceroute \
  netcat-openbsd \
  iperf \
  iperf3 \
  less \
  screen \
  tmux \
  ncurses-term \
  time \
  file \
  rsync \
  unison \
  sshfs \
  vim \
  pv \
  bind9-host \
  dnsutils \
  aspell \
  aspell-en \
  git \
  tig \
  subversion \
  brz \
  cvs \
  unzip \
  bzip2 \
  perl \
  python \
  python3 \
  python-dev \
  python3-dev \
  ruby \
  rake \
  bundler \
  ruby-dev \
  rustc \
  cargo \
  golang-go \
  elinks \
  gawk \
  wget \
  curl \
  libcurl4-openssl-dev \
  libssl-dev \
  libmbedtls-dev \
  libgnutls28-dev \
  libncurses5-dev \
  zlib1g-dev \
  build-essential \
  libtool-bin \
  autogen \
  devscripts \
  gobjc \
  gobjc++ \
  gfortran \
  cmake \
  gdb \
  strace \
  pkg-config \
  bison \
  flex \
  ragel \
  gettext \
  groff \
  dos2unix \
  elfutils \
  libelf-dev \
  libmpfr-dev \
  libmpc-dev \
  libffi-dev \
  libglib2.0-dev \
  libbz2-dev \
  liblzma-dev \
  libboost-all-dev \
  libssl-dev \
  libpcap-dev \
  libsqlite3-dev \
  libpcre3-dev \
  libnl-3-dev \
  libnl-genl-3-dev \
  libxext-dev \
  libsys-cpu-perl \
  libossp-uuid-dev \
  dejagnu \
  quilt \
  xz-utils \
  zstd \
  tcl \
  apache2-utils \
  imagemagick \
  help2man \
  python3-pexpect \
  virtualenv \
  clang \
  python3-clang \
  clang-9 \
  clang-11 \
  ninja-build \
  emacs-nox \
  liblz4-tool \
  libgmp-dev \
  liblog4cplus-dev \
  libsodium-dev \
  qt5-qmake \
  rng-tools \
  mosh \
  gnat \
  python3-pip \
  libcmocka-dev \
  texinfo \
  creduce \
  extra-cmake-modules \
  libradcli-dev \
  linux-perf \
  capnproto \
  libcapnp-dev \
  libbotan-2-dev \
  python3-botan \
  python-is-python2 \
  gcc-9 \
  g++-9 \
  gcc-10 \
  g++-10 \
  && rm -rf /var/lib/apt/lists/* /var/cache/apt/archives/*.deb

# motd prompt when connecting over SSH
ADD motd /etc/motd
EOF

docker build -t "$TARGET_IMAGE" "$CONTEXT"
rm -r "$CONTEXT"

echo "* Generated docker image $TARGET_IMAGE"
