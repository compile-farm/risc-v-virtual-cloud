#!/bin/sh

set -e

MOUNTPOINT="/mnt/fedora-docker-import"

import_fedora() {
    local IMAGE="$1"
    local DOCKER_TAG="$2"
    local VERSION="$3"
    local URL=https://dl.fedoraproject.org/pub/alt/risc-v/repo/virt-builder-images/images/"$IMAGE".xz

    echo "* Downloading Fedora image..."
    wget --continue "$URL"
    
    echo "* Uncompressing Fedora image..."
    xz --decompress --keep -T0 "$IMAGE".xz || true
    
    echo "* Determining image structure..."
    # partx can output with a format like START=1234, perfect for shell.
    # Don't do this at home.
    eval $(partx --pairs --nr 4 -o START "$IMAGE")
    [ -z "$START" ] && { echo "Error running partx"; exit 1; }
    
    echo "* Mounting Fedora image in $MOUNTPOINT..."
    mkdir -p "$MOUNTPOINT"
    # Try to unmount just in case a previous run of the script failed.
    umount "$MOUNTPOINT" 2>/dev/null || true
    # Also don't mount untrusted filesystems at home ;)
    mount -o loop -o offset=$((START*512)) "$IMAGE" "$MOUNTPOINT"
    
    echo "* Importing image content in docker..."
    
    cd "$MOUNTPOINT"
    tar c . | docker image import -c "CMD /bin/bash" - "$DOCKER_TAG":"$VERSION"

    echo "* Tagging as ${DOCKER_TAG}:${VERSION} and ${DOCKER_TAG}:latest"
    docker tag "$DOCKER_TAG":"$VERSION""$DOCKER_TAG":latest
    
    echo "* Unmounting Fedora image"
    cd
    umount "$MOUNTPOINT"
}

import_fedora Fedora-Minimal-Rawhide-20200108.n.0-sda.raw fedora-riscv64-minimal 20200108 || true
import_fedora Fedora-Developer-Rawhide-20200108.n.0-sda.raw fedora-riscv64-developer 20200108
