#!/bin/sh
#
# Unlike Debian, this is a common build script for both -min and -full images.
# This is because Fedora already provides these two variants, so we
# apply the same basic changes to both variants.
# We simply install a few additional packages in the -full variant.

SCRIPTDIR=$(dirname "$0")

VARIANT="$1"

[ "$VARIANT" = "min" ] && BASE_IMAGE=fedora-riscv64-minimal
[ "$VARIANT" = "full" ] && BASE_IMAGE=fedora-riscv64-developer

[ -z "$BASE_IMAGE" ] && { echo "usage: $0 {min,full}"; exit 1; }

TARGET_IMAGE=fedora-riscv64-gccfarm-"$VARIANT"

ROOT_SSH_KEY=""

echo "* Customizing docker image $BASE_IMAGE..."

CONTEXT="$(mktemp -d)"
[ -z "$CONTEXT" ] && exit 1

# Build motd from template
sed -e "s#%IMAGE%#${TARGET_IMAGE}#g" \
    -e "s#%BUILD_DATE%#$(date +%Y-%m-%d)#g" \
    -e "s#%SOURCE%#https://framagit.org/compile-farm/risc-v-virtual-cloud#g" \
    -e "s#%HOST%#gcc140#g" \
    "$SCRIPTDIR"/../motd > "$CONTEXT"/motd

# Add SSH host keys
cp -p "$SCRIPTDIR"/../ssh-host-keys/ssh* "$CONTEXT"/

cat > "$CONTEXT"/Dockerfile <<EOF
FROM $BASE_IMAGE:latest
# Necessary to run sshd directly
RUN mkdir -p /run/sshd

# Copy SSH host keys
ADD ssh* /etc/ssh/

# Copy authorized_keys for root
RUN mkdir -p /root/.ssh && echo "$ROOT_SSH_KEY" > /root/.ssh/authorized_keys

# motd prompt when connecting over SSH
ADD motd /etc/motd
EOF

# Add more packages for the full image
# FIXME: Disabled because the image is too old and it doesn't work (dependency issues)
false && [ "$VARIANT" = "full" ] && cat >> "$CONTEXT"/Dockerfile <<EOF
# Start by upgrading everything
RUN yum upgrade -y && yum clean packages

# Install additional packages
RUN yum install -y \
  csh \
  tcsh \
  zsh \
  traceroute \
  unison \
  tig \
  mercurial \
  bzr \
  && yum clean all
EOF

docker build -t "$TARGET_IMAGE":latest "$CONTEXT"
rm -r "$CONTEXT"

echo "* Generated docker image $TARGET_IMAGE"
