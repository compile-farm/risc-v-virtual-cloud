# Setting up a physical machine

We use `libvirt` on Fedora.  This was tested on Fedora 30.

## Install required packages

    dnf install libvirt virt-install libguestfs-tools-c qemu-system-riscv bridge-utils virt-top

## Configure network

On the OSUOSL machines, we bridge all VMs to the physical network because
there is a DHCP server on the network.

Follow the [guide for bridged networking in libvirt](https://wiki.libvirt.org/page/Networking#Bridged_networking_.28aka_.22shared_physical_device.22.29>)


# Adding a new virtual machine

## Customisation

We want to apply the following customisation to all virtual machine images:

- Add an external mountpoint for `/home`, so that user data will survive when we rebuild a new image.
  It can be either a local LVM volume, a NFS mountpoint, a 9p shared filesystem, a distributed filesystem...
- Add all user accounts from the compile farm (`/etc/passwd`, groups, etc).
- Add known SSH host keys, so that users won't get SSH warnings when connecting after we rebuild an image.
- Install basic packages that users will need: compiler, text editor, screen/tmux, etc.
  We already have an ansible role to do that on all compile farm machines.
  For ansible to work, we need at least: `python libselinux-python`

## Building a Fedora image

TODO (script)
